import {useState} from 'react';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {

  // Deconstruct the course properties into their own variables
    const { name, description, price} = courseProp;

  //Syntax:
  //const [getter, setter] = useState(initialGetterValue)
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [disabled, setDisabled] = useState(false);

  //Function that keeps track of the enrollees for a course
    function enroll(){
      if (count < seats) {
        setCount(count + 1);
        console.log('Enrollees ' + count);
      } else {
        alert("no more seats");
        setDisabled(true);
      }
    }

    return (
      <Card>
          <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>
              <Card.Text>{count} Enrollees</Card.Text>
              <Button variant="primary" onClick={enroll} disabled={disabled}>
                Enroll
              </Button>
          </Card.Body>
      </Card>  

    )
}


